' Author: Leonardo Cecchi <leonardoce@interfree.it>
' 
' This is free and unencumbered software released into the public domain.
' 
' Anyone is free to copy, modify, publish, use, compile, sell, or
' distribute this software, either in source code form or as a compiled
' binary, for any purpose, commercial or non-commercial, and by any
' means.
' 
' In jurisdictions that recognize copyright laws, the author or authors
' of this software dedicate any and all copyright interest in the
' software to the public domain. We make this dedication for the benefit
' of the public at large and to the detriment of our heirs and
' successors. We intend this dedication to be an overt act of
' relinquishment in perpetuity of all present and future rights to this
' software under copyright law.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
' EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
' MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
' IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
' OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
' ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
' OTHER DEALINGS IN THE SOFTWARE.
' 
' For more information, please refer to <http://unlicense.org/>

type mg_context as any ptr
type mg_connection as any ptr

' This structure needs to be passed to mg_start(), to let mongoose know
' which callbacks to invoke. For detailed description, see
' https://github.com/valenok/mongoose/blob/master/UserManual.md
type mg_callbacks 
    begin_request as function cdecl (as mg_connection) as integer
    end_request as sub cdecl (as mg_connection, reply_status_code as integer)
    log_message as function cdecl (as mg_connection, message as zstring) as integer
    init_ssl as function cdecl (ssl_context as any ptr) as integer
    websocket_connect as function cdecl (as mg_connection) as integer
    websocket_ready as function cdecl (as mg_connection) as integer
    websocket_data as function cdecl (as mg_connection) as integer
    open_file as function cdecl (as mg_connection, path as zstring ptr, data_len as integer) as zstring ptr
    init_lua as function cdecl (as mg_connection, lua_context as any ptr) as any ptr
    upload as sub cdecl     (as mg_connection, file_name as zstring ptr)
end type

' This structure contains information about the HTTP headers. 
type mg_header 
    name as zstring ptr           ' HTTP header name
    value as zstring ptr          ' HTTP header value
end type

' This structure contains information about the HTTP request.
type mg_request_info
    request_method as zstring ptr ' "GET", "POST", etc
    uri as zstring ptr            ' URL-decoded URI
    http_version as zstring ptr   ' E.g. "1.0", "1.1"
    query_string as zstring ptr   ' URL part after '?', not including '?', or NULL
    remote_user as zstring ptr    ' Authenticated user, or NULL if no auth used
    remote_ip as long             ' Client's IP address
    remote_port as integer        ' Client's port
    is_ssl as integer             ' 1 if SSL-ed, 0 if not
    user_data as any ptr          ' User data pointer passed to mg_start()

    num_headers as integer        ' Number of HTTP headers
    http_headers(64) as mg_header
end type


' Start web server.
'
' Parameters:
'   callbacks: mg_callbacks structure with user-defined callbacks.
'   options: NULL terminated list of option_name, option_value pairs that
'            specify Mongoose configuration parameters.
'
' Side-effects: on UNIX, ignores SIGCHLD and SIGPIPE signals. If custom
'    processing is required for these, signal handlers must be set up
'    after calling mg_start().
'
'
' Example:
'   const char *options[] = {
'     "document_root", "/var/www",
'     "listening_ports", "80,443s",
'     NULL
'   };
'   struct mg_context *ctx = mg_start(&my_func, NULL, options);
'
' Please refer to http://code.google.com/p/mongoose/wiki/MongooseManual
' for the list of valid option and their possible values.
'
' Return:
'   web server context, or NULL on error.
declare function mg_start cdecl lib "mongoose" alias "mg_start" _
    (as mg_callbacks ptr, as any ptr, as zstring ptr ptr) as mg_context

' Stop the web server.
'
' Must be called last, when an application wants to stop the web server and
' release all associated resources. This function blocks until all Mongoose
' threads are stopped. Context pointer becomes invalid.
declare sub mg_stop cdecl lib "mongoose" alias "mg_stop" (as mg_context)

' Get the value of particular configuration parameter.
' The value returned is read-only. Mongoose does not allow changing
' configuration at run time.
' If given parameter name is not valid, NULL is returned. For valid
' names, return value is guaranteed to be non-NULL. If parameter is not
' set, zero-length string is returned.
declare function mg_get_option cdecl lib "mongoose" alias "mg_get_option" _
    (as mg_context, name as zstring ptr) as zstring ptr

' Return array of strings that represent valid configuration options.
' For each option, a short name, long name, and default value is returned.
' Array is NULL terminated.
declare function mg_get_valid_option_names cdecl lib "mongoose" _
    alias "mg_get_valid_option_names" () as zstring ptr ptr

' Return information associated with the request.
declare function mg_get_request_info cdecl lib "mongoose" _
    alias "mg_get_request_info" (as mg_connection) as mg_request_info ptr 

' Add, edit or delete the entry in the passwords file.
'
' This function allows an application to manipulate .htpasswd files on the
' fly by adding, deleting and changing user records. This is one of the
' several ways of implementing authentication on the server side. For another,
' cookie-based way please refer to the examples/chat.c in the source tree.
'
' If password is not NULL, entry is added (or modified if already exists).
' If password is NULL, entry is deleted.
'
' Return:
'   1 on success, 0 on error.
declare function mg_modify_passwords_file cdecl lib "mongoose" _
    alias "mg_modify_passwords_file" _
    (passwords_file_name as zstring ptr, _
     domain as zstring ptr, _
     user as zstring ptr, _
     password as zstring ptr) as integer

' Send data to the client.
' Return:
'  0   when the connection has been closed
'  -1  on error
'  number of bytes written on success
declare function mg_write cdecl lib "mongoose" alias "mg_write" _
    (as mg_connection, _
    buf as any ptr, l as integer) as integer
     
' Send contents of the entire file together with HTTP headers.
declare sub mg_send_file cdecl lib "mongoose"  alias "mg_send_file" _
    (as mg_connection ptr, path as zstring ptr)
 
' Read data from the remote end, return number of bytes read.
declare function mg_read cdecl lib "mongoose" alias "mg_read" _
    (as mg_connection ptr, buf as any ptr, l as integer) as integer

' Get the value of particular HTTP header.
'
' This is a helper function. It traverses request_info->http_headers array,
' and if the header is present in the array, returns its value. If it is
' not present, NULL is returned.
declare function mg_get_header cdecl lib "mongoose" alias "mg_get_header" _
    (as mg_connection ptr, name as zstring ptr) as zstring ptr

' Get a value of particular form variable.
'
' Parameters:
'   data: pointer to form-uri-encoded buffer. This could be either POST data,
'         or request_info.query_string.
'   data_len: length of the encoded data.
'   var_name: variable name to decode from the buffer
'   dst: destination buffer for the decoded variable
'   dst_len: length of the destination buffer
'
' Return:
'   On success, length of the decoded variable.
'   On error:
'      -1 (variable not found).
'      -2 (destination buffer is NULL, zero length or too small to hold the decoded variable).
'
' Destination buffer is guaranteed to be '\0' - terminated if it is not
' NULL or zero length.
declare function mg_get_var cdecl lib "mongoose" alias "mg_get_var" _
    (data as zstring ptr, data_len as integer, _
    var_name as zstring ptr, dst as zstring ptr, dst_len as integer) _
    as integer

' Fetch value of certain cookie variable into the destination buffer.
'
' Destination buffer is guaranteed to be '\0' - terminated. In case of
' failure, dst[0] == '\0'. Note that RFC allows many occurrences of the same
' parameter. This function returns only first occurrence.
'
' Return:
'   On success, value length.
'   On error:
'      -1 (either "Cookie:" header is not present at all or the requested parameter is not found).
'      -2 (destination buffer is NULL, zero length or too small to hold the value).
declare function mg_get_cookie cdecl lib "mongoose" alias "mg_get_cookie" _
    (as mg_connection ptr, _
    cookie_name as zstring ptr, _ 
    buf as zstring ptr, _ 
    buf_len as integer) as integer


' Download data from the remote web server.
'   host: host name to connect to, e.g. "foo.com", or "10.12.40.1".
'   port: port number, e.g. 80.
'   use_ssl: wether to use SSL connection.
'   error_buffer, error_buffer_size: error message placeholder.
'   request_fmt,...: HTTP request.
' Return:
'   On success, valid pointer to the new connection, suitable for mg_read().
'   On error, NULL. error_buffer contains error message.
' Example:
'   char ebuf[100];
'   struct mg_connection *conn;
'   conn = mg_download("google.com", 80, 0, ebuf, sizeof(ebuf),
'                      "%s", "GET / HTTP/1.0\r\nHost: google.com\r\n\r\n");
declare function mg_download cdecl lib "mongoose" alias "mg_download" _
    (host as zstring ptr, port as integer, use_ssl as integer, _
    error_buffer as zstring ptr, error_buffer_size as integer, _
    request_fmt as zstring ptr) as mg_connection


' Close the connection opened by mg_download().
declare sub mg_close_connection cdecl lib "mongoose" alias "mg_close_connection" _
    (as mg_connection)


' File upload functionality. Each uploaded file gets saved into a temporary
' file and MG_UPLOAD event is sent.
' Return number of uploaded files.
declare function mg_upload cdecl lib "mongoose" alias "mg_upload" _
    (as mg_connection, destination_dir as zstring ptr) _
    as integer


' Convenience function -- create detached thread.
' Return: 0 on success, non-0 on error.
type mg_thread_func_t as function cdecl (as any ptr) as any ptr
declare function mg_start_thread cdecl lib "mongoose" alias "mg_start_thread" _
    (f as mg_thread_func_t, p as any ptr) as integer


' Return builtin mime type for the given file name.
' For unrecognized extensions, "text/plain" is returned.
declare function mg_get_builtin_mime_type cdecl lib "mongoose" alias _
    "mg_get_builtin_mime_type" (file_name as zstring ptr) as zstring ptr

' Return Mongoose version.
declare function mg_version cdecl lib "mongoose" alias "mg_version" () as zstring ptr
    


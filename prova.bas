' Author: Leonardo Cecchi <leonardoce@interfree.it>
' 
' This is free and unencumbered software released into the public domain.
' 
' Anyone is free to copy, modify, publish, use, compile, sell, or
' distribute this software, either in source code form or as a compiled
' binary, for any purpose, commercial or non-commercial, and by any
' means.
' 
' In jurisdictions that recognize copyright laws, the author or authors
' of this software dedicate any and all copyright interest in the
' software to the public domain. We make this dedication for the benefit
' of the public at large and to the detriment of our heirs and
' successors. We intend this dedication to be an overt act of
' relinquishment in perpetuity of all present and future rights to this
' software under copyright law.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
' EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
' MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
' IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
' OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
' ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
' OTHER DEALINGS IN THE SOFTWARE.
' 
' For more information, please refer to <http://unlicense.org/>

#include once "mongoose.bi" 

sub clear_callbacks(byref cbk as mg_callbacks)
    cbk.begin_request = 0
    cbk.end_request = 0
    cbk.log_message = 0
    cbk.init_ssl = 0
    cbk.websocket_connect = 0
    cbk.websocket_ready = 0
    cbk.websocket_data = 0
    cbk.open_file = 0
    cbk.init_lua = 0
    cbk.upload = 0
end sub

sub prendi_nomi_opzioni()
    dim stato as zstring ptr ptr
    dim i as integer
    
    i = 0
    stato = mg_get_valid_option_names()
    while 1
        if stato[i]=0 then
            exit while
        end if
        
        print "nome corto:", *stato[i]
        print "nome lungo:", *stato[i+1]
        print "default   :", *stato[i+2]
        print "--"
        i+=3
    wend
end sub

sub main()
    dim ctx as mg_context
    dim cbk as mg_callbacks   
    dim ciao as string
    
    clear_callbacks(cbk)
    
    ctx = mg_start(@cbk, 0, 0)
    input ciao
    print ctx
    mg_stop ctx
end sub

prendi_nomi_opzioni()
main()
